# Class: default

## Constructors

### constructor

• **new default**(`version?`, `language?`, `baseurl?`)

Create a new instance of Ddragon

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `version` | `string` | `"9.22.1"` | The version that will be used for generating URLs |
| `language` | [`Language`](../docs.md#language) | `"en_US"` | The language that will be used for generating URLs |
| `baseurl` | `string` | `"https://ddragon.leagueoflegends.com"` | The baseurl that will be used for generating URLs |

## Properties

### data

• **data**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `champion` | (`name`: `string`) => `string` |
| `champions` | () => `string` |
| `championsFull` | () => `string` |
| `item` | () => `string` |
| `language` | () => `string` |
| `map` | () => `string` |
| `missionAssets` | () => `string` |
| `profileicon` | () => `string` |
| `runes` | () => `string` |
| `summoner` | () => `string` |

___

### images

• **images**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `centered` | (`name`: `string`, `num`: `number`) => `string` |
| `champion` | (`full`: `string`) => `string` |
| `item` | (`full`: `string`) => `string` |
| `loading` | (`name`: `string`, `num`: `number`) => `string` |
| `map` | (`full`: `string`) => `string` |
| `mission` | (`full`: `string`) => `string` |
| `passive` | (`full`: `string`) => `string` |
| `profileicon` | (`full`: `string`) => `string` |
| `rune` | (`icon`: `string`) => `string` |
| `spell` | (`full`: `string`) => `string` |
| `splash` | (`name`: `string`, `num`: `number`) => `string` |
| `sprite` | (`sprite`: `string`) => `string` |
| `statMod` | (`statName`: `string`) => `string` |
| `summoner` | (`full`: `string`) => `string` |
| `tile` | (`name`: `string`, `num`: `number`) => `string` |

## Methods

### dragontail

▸ **dragontail**(): `string`

A compressed tarball (.tgz) which will contain all assets for a patch.

#### Returns

`string`

___

### languages

▸ **languages**(): `string`

All supported languages.

#### Returns

`string`

___

### versions

▸ **versions**(): `string`

You can find all valid Data Dragon versions in the versions file. Typically
there's only a single build of Data Dragon for a given patch, however
occasionally there will be additional builds. This typically occurs when
there's an error in the original build. As such, you should always use the
most recent Data Dragon version for a given patch for the best results.

The latest version is always the first element in the array.

#### Returns

`string`
