import Dd from "../src/index";

const version = "1.1.1";
const language = "en_GB";
const baseurl = "https://test.com";
const dd = new Dd(version, language, baseurl);

describe("defaults", () => {
  const defaultDragon = new Dd();
  it("uses https", () => {
    const championsUrl = defaultDragon.data.champions();
    expect(championsUrl).toContain("https");
  });
});

describe("constructor", () => {
  it("passes values", () => {
    const championsUrl = dd.data.champions();
    expect(championsUrl).toContain(version);
    expect(championsUrl).toContain(language);
    expect(championsUrl).toContain(baseurl);
  });
});

describe("versions", () => {
  it("returns the correct url", () => {
    expect(dd.versions()).toEqual(`${baseurl}/api/versions.json`);
  });
});
