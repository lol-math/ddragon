## [3.5.2](https://gitlab.com/lol-math/ddragon/compare/v3.5.1...v3.5.2) (2023-11-16)


### Bug Fixes

* use normal name for magic resist again as riot has fixed the name in dragontail ([405448e](https://gitlab.com/lol-math/ddragon/commit/405448e050d6b52dfe0bb74e5380e9a80296f590))

## [3.5.1](https://gitlab.com/lol-math/ddragon/compare/v3.5.0...v3.5.1) (2022-11-20)


### Bug Fixes

* use the `MagicResist_Fix` version of the magic resist stat mod icon. ([f97425d](https://gitlab.com/lol-math/ddragon/commit/f97425d30774fea90e1ae080ad272d8c8e7bf443))

# [3.5.0](https://gitlab.com/lol-math/ddragon/compare/v3.4.0...v3.5.0) (2022-11-07)


### Features

* centered image ([e3a6472](https://gitlab.com/lol-math/ddragon/commit/e3a6472c559147643bbd227339703ac10e71d025))

# [3.4.0](https://gitlab.com/lol-math/ddragon/compare/v3.3.4...v3.4.0) (2022-07-17)


### Bug Fixes

* make use of changed fiddlesticks endpoints ([f41a630](https://gitlab.com/lol-math/ddragon/commit/f41a6305c4d42c12bc5425dc0464f7e11b72fd99))


### Features

* add dragontail URL ([90734df](https://gitlab.com/lol-math/ddragon/commit/90734dff8e19e5e6e1b9d4601e9fc9fa4ad7c056))
* export languages type ([5fd7fdd](https://gitlab.com/lol-math/ddragon/commit/5fd7fdd69dca78099f1409cae258406ff5251298))

# 1.0.0-next.1 (2022-07-17)


### Bug Fixes

* make use of changed fiddlesticks endpoints ([f41a630](https://gitlab.com/lol-math/ddragon/commit/f41a6305c4d42c12bc5425dc0464f7e11b72fd99))


### Features

* add dragontail URL ([90734df](https://gitlab.com/lol-math/ddragon/commit/90734dff8e19e5e6e1b9d4601e9fc9fa4ad7c056))
* export languages type ([5fd7fdd](https://gitlab.com/lol-math/ddragon/commit/5fd7fdd69dca78099f1409cae258406ff5251298))
